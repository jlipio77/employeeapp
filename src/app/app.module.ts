import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './component/header/header.component';
import { ViewEmployeeComponent } from './component/view-employee/view-employee.component';
import { AddEmployeeComponent } from './component/add-employee/add-employee.component';
import { EditEmployeeComponent } from './component/edit-employee/edit-employee.component';
import { EditSupervisorComponent } from './component/edit-supervisor/edit-supervisor.component';
import { AddSupervisorComponent } from './component/add-supervisor/add-supervisor.component';
import { ViewSupervisorComponent } from './component/view-supervisor/view-supervisor.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { EmployeeService } from './service/employee.service';
import { SupervisorService } from './service/supervisor.service';
import { HttpClientModule } from '@angular/common/http';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './component/home/home.component';
import { DetailsEmployeeComponent } from './component/details-employee/details-employee.component';
import { DetailsSupervisorComponent } from './component/details-supervisor/details-supervisor.component';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ViewEmployeeComponent,
    AddEmployeeComponent,
    EditEmployeeComponent,
    EditSupervisorComponent,
    AddSupervisorComponent,
    ViewSupervisorComponent,
    HomeComponent,
    DetailsEmployeeComponent,
    DetailsSupervisorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    HttpClientModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatSelectModule,
    MatIconModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    

   
  ],
  providers: [EmployeeService, SupervisorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
