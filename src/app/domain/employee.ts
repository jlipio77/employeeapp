import { Supervisor } from './supervisor';

export class Employee {
    public id: number;
    public firstName: string;
    public lastName: string;
    public gender: string;
    public salary: number;
    public resigned: boolean;
    public supervisor: Supervisor;

}
