export class Supervisor {
    public id: number;
    public firstName: string;
    public lastName: string;
    public gender: string;
}
