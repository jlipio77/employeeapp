import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/domain/employee';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';

@Component({
  selector: 'app-details-employee',
  templateUrl: './details-employee.component.html',
  styleUrls: ['./details-employee.component.css']
})
export class DetailsEmployeeComponent implements OnInit {

  id: number;
  employeeDetail: Employee;

  constructor(private route: ActivatedRoute, private router: Router, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeDetail = new Employee();
    this.id = this.route.snapshot.params['id'];
    this.employeeService.getEmployeeById(this.id)
    .subscribe(data => {
      console.log(data)
      this.employeeDetail = data;
    }, error => console.log(error));
  }

}
