import { Component, OnInit, ViewChild } from '@angular/core';
import { Supervisor } from 'src/app/domain/supervisor';
import { Employee } from 'src/app/domain/employee';
import { ActivatedRoute, Router } from '@angular/router';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { EmployeeService } from 'src/app/service/employee.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-details-supervisor',
  templateUrl: './details-supervisor.component.html',
  styleUrls: ['./details-supervisor.component.css']
})
export class DetailsSupervisorComponent implements OnInit {

  id: number;
  supervisorDetail: Supervisor;
  employeeList: Employee[];


  displayedColumns = ['id', 'firstName', 'lastName', 'gender', 'salary', 'resigned'];
  dataSource = new MatTableDataSource<Employee>();
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  constructor(private route: ActivatedRoute, private router: Router, private supervisorService: SupervisorService, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.supervisorDetail = new Supervisor();
    this.id = this.route.snapshot.params['id'];
    this.supervisorService.getSupervisorById(this.id)
    .subscribe(data => {
      console.log(data)
      this.supervisorDetail = data;
    }, error => console.log(error));


      this.employeeService.getEmployeesBySupervisorId(this.id).
      subscribe(emps=>{
        
        this.employeeList=emps;
        this.dataSource.data = this.employeeList;
        console.log(this.employeeList)
      });
  }
}
