import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsSupervisorComponent } from './details-supervisor.component';

describe('DetailsSupervisorComponent', () => {
  let component: DetailsSupervisorComponent;
  let fixture: ComponentFixture<DetailsSupervisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsSupervisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsSupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
