import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { Supervisor } from 'src/app/domain/supervisor';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-supervisor',
  templateUrl: './add-supervisor.component.html',
  styleUrls: ['./add-supervisor.component.css']
})
export class AddSupervisorComponent implements OnInit {
    supervisorForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),

  });

  constructor(private supervisorService: SupervisorService, private snackbar: MatSnackBar) { }

  sup: Supervisor;

  ngOnInit() {
    }
  

  addSupervisor(){
    this.sup = {id:0,
      firstName:this.supervisorForm.controls['firstName'].value,
      lastName:this.supervisorForm.controls['lastName'].value,
      gender:this.supervisorForm.controls['gender'].value
  }
  
  this.supervisorService.addSupervisor(this.sup)
  .subscribe (supervisor => {console.log(supervisor); this.supervisorForm.reset();
    this.snackbar.open('New supervisor successfully added!' , 'Close', {duration: 10000});});
}
}
