import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { Supervisor } from 'src/app/domain/supervisor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-supervisor',
  templateUrl: './edit-supervisor.component.html',
  styleUrls: ['./edit-supervisor.component.css']
})
export class EditSupervisorComponent implements OnInit {

  id: number;

  supervisorForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),

  });

  constructor(private supervisorService: SupervisorService, private route: ActivatedRoute) { }

  editSup: Supervisor

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    
   this.supervisorService.getSupervisorById(+this.id).subscribe(supervisor=>{
     this.editSup=supervisor
     console.log(this.editSup)
    this.supervisorForm.controls['firstName'].setValue(this.editSup.firstName);
    this.supervisorForm.controls['lastName'].setValue(this.editSup.lastName);
    this.supervisorForm.controls['gender'].setValue(this.editSup.gender);
   })
  }

  xeditSupervisor(){
    alert("Successfully Updated!")
    this.editSup={
      id:0,
      firstName:this.supervisorForm.controls['firstName'].value,
      lastName:this.supervisorForm.controls['lastName'].value,
      gender:this.supervisorForm.controls['gender'].value,
      }
  
      this.supervisorService.editSupervisor(this.id, this.editSup)
      .subscribe (supervisor => {
        console.log(supervisor); this.supervisorForm.reset();});
    } 

  

}
