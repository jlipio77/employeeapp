import { Component, OnInit, ViewChild } from '@angular/core';
import { Supervisor } from 'src/app/domain/supervisor';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-view-supervisor',
  templateUrl: './view-supervisor.component.html',
  styleUrls: ['./view-supervisor.component.css']
})
export class ViewSupervisorComponent implements OnInit {
  supervisors: Supervisor[] = [];
  displayedColumns = ['id', 'firstName', 'lastName', 'gender'];
  dataSource = new MatTableDataSource<Supervisor>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  applyFilter1(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private supervisorService: SupervisorService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.supervisorService.getSupervisors()
    .subscribe (supervisors => {this.supervisors = supervisors; this.dataSource.data =  this.supervisors; this.dataSource.paginator = this.paginator; this.dataSource.sort = this.sort});
  }

  xdeleteSupervisor(id: number){
    this.supervisorService.deleteSupervisor(id)
    .subscribe(s => window.location.reload());
    this.snackbar.open('Supervisor has been removed' , 'Close', {duration: 10000});
  }

}
