import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/service/employee.service';
import { ActivatedRoute } from '@angular/router';
import { Employee } from 'src/app/domain/employee';
import { FormGroup, FormControl } from '@angular/forms';
import { Supervisor } from 'src/app/domain/supervisor';
import { SupervisorService } from 'src/app/service/supervisor.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  id: number;

  employeeForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    salary: new FormControl(''),
    resigned: new FormControl(''),
    supervisor_id: new FormControl('')

  });

  constructor(private employeeService: EmployeeService, private supervisorService: SupervisorService, private route: ActivatedRoute) { }
  supervisors: Supervisor[] = [];
  
  editEmp: Employee;

  ngOnInit() {
  this.supervisorService.getSupervisors()
  .subscribe(s => {
    this.supervisors = s;
  });
  

    this.id = +this.route.snapshot.paramMap.get('id');
    
   this.employeeService.getEmployeeById(+this.id).subscribe(employee=>{
     this.editEmp=employee
     console.log(this.editEmp)
    this.employeeForm.controls['firstName'].setValue(this.editEmp.firstName);
    this.employeeForm.controls['lastName'].setValue(this.editEmp.lastName);
    this.employeeForm.controls['gender'].setValue(this.editEmp.gender);
    this.employeeForm.controls['salary'].setValue(this.editEmp.salary);
    this.employeeForm.controls['resigned'].setValue(this.editEmp.resigned);
    
   })

   
    
  }

  xeditEmployee(){
    alert("Successfully Updated!")
    this.editEmp={
      id:0,
      firstName:this.employeeForm.controls['firstName'].value,
      lastName:this.employeeForm.controls['lastName'].value,
      gender:this.employeeForm.controls['gender'].value,
      salary:+this.employeeForm.controls['salary'].value,
      resigned:this.employeeForm.controls['resigned'].value,
      supervisor:{
        id:+this.employeeForm.controls['supervisor_id'].value,
        firstName:'',
        lastName:'',
        gender:''
      }}
  
      this.employeeService.editEmployee(this.id, this.editEmp)
      .subscribe (employee => {
        console.log(employee); this.employeeForm.reset();});
    } 
  }

