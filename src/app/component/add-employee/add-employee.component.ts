import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from 'src/app/service/employee.service';
import { Supervisor } from 'src/app/domain/supervisor';
import { SupervisorService } from 'src/app/service/supervisor.service';
import { Employee } from 'src/app/domain/employee';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employeeForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    salary: new FormControl(''),
    resigned: new FormControl(false),
    supervisor_id: new FormControl('')

  });

  constructor(private employeeService: EmployeeService, private supervisorService: SupervisorService, private snackbar: MatSnackBar) { }

  supervisors: Supervisor[] = [];
  
  emp: Employee;

  ngOnInit() {
  this.supervisorService.getSupervisors()
  .subscribe(s => {
    this.supervisors = s;
  });
  }

  addEmployee(){
    this.emp={id:0,
    firstName:this.employeeForm.controls['firstName'].value,
    lastName:this.employeeForm.controls['lastName'].value,
    gender:this.employeeForm.controls['gender'].value,
    salary:+this.employeeForm.controls['salary'].value,
    resigned:false,
    supervisor:{
      id:+this.employeeForm.controls['supervisor_id'].value,
      firstName:'',
      lastName:'',
      gender:''
    }}

    this.employeeService.addEmployee(this.emp)
    .subscribe (employee => {
      console.log(employee); this.employeeForm.reset();
      this.snackbar.open('New employee successfully added!' , 'Close', {duration: 10000});});
  }

}
