import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Employee } from 'src/app/domain/employee';
import { EmployeeService } from 'src/app/service/employee.service';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {
  employees: Employee[] = [];
  displayedColumns = ['id', 'firstName', 'lastName', 'gender', 'salary', 'resigned', 'supervisor' ];
  dataSource = new MatTableDataSource<Employee>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private employeeService: EmployeeService, private snackbar: MatSnackBar) { } 

  ngOnInit() {
    this.employeeService.getEmployees()
    .subscribe(employees => {this.employees = employees; this.dataSource.data = this.employees; this.dataSource.paginator = this.paginator; this.dataSource.sort = this.sort});
  }


  xdeleteEmployee(id: number){
    this.employeeService.deleteEmployee(id)
    .subscribe(s => window.location.reload());
    this.snackbar.open('Employee has been removed' , 'Close', {duration: 10000});
  }

}
