import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Supervisor } from '../domain/supervisor';

@Injectable({
  providedIn: 'root'
})
export class SupervisorService {

  private url = "http://localhost:8080/supervisors";

  constructor(private http: HttpClient) { }

  getSupervisors(): Observable<Supervisor[]> {
    return this.http.get<Supervisor[]>(this.url);
  }

  addSupervisor(supervisor: Supervisor): Observable<Supervisor>{
    return this.http.post<Supervisor>(this.url, supervisor);
  }

  deleteSupervisor(id: number): Observable<number>{
    return this.http.delete<number>(this.url + '/delete?id=' +id);
  }

  editSupervisor(id: number, supervisor: Supervisor): Observable<Supervisor>{
    return this.http.put<Supervisor>(this.url + '/edit/?id=' + id, supervisor);
  }

  getSupervisorById(id: number): Observable<Supervisor>{
    return this.http.get<Supervisor>(this.url + '/view?id=' + id);
  }
}
