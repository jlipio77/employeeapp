import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../domain/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url = "http://localhost:8080/employees";

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.url);
  }
  
  getEmployeesBySupervisorId(id: number): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.url+'/list?id='+id);
  }

  addEmployee(employee: Employee): Observable<Employee>{
    return this.http.post<Employee>(this.url, employee);
  }

  deleteEmployee(id: number): Observable<number>{
    return this.http.delete<number>(this.url + '/delete?id=' +id);
  }

  editEmployee(id: number, employee: Employee): Observable<Employee>{
    return this.http.put<Employee>(this.url + '/edit/?id=' + id, employee);
  }

  getEmployeeById(id: number): Observable<Employee>{
    return this.http.get<Employee>(this.url + '/view?id=' + id);
  }
}
