import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewEmployeeComponent } from './component/view-employee/view-employee.component';
import { ViewSupervisorComponent } from './component/view-supervisor/view-supervisor.component';
import { HomeComponent } from './component/home/home.component';
import { AddEmployeeComponent } from './component/add-employee/add-employee.component';
import { AddSupervisorComponent } from './component/add-supervisor/add-supervisor.component';
import { EditEmployeeComponent } from './component/edit-employee/edit-employee.component';
import { EditSupervisorComponent } from './component/edit-supervisor/edit-supervisor.component';
import { DetailsEmployeeComponent } from './component/details-employee/details-employee.component';
import { DetailsSupervisorComponent } from './component/details-supervisor/details-supervisor.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'emp', component: ViewEmployeeComponent},
  {path: 'sup', component: ViewSupervisorComponent},
  {path: 'emp/add', component: AddEmployeeComponent},
  {path: 'sup/add', component: AddSupervisorComponent},
  {path: 'emp/edit/:id', component: EditEmployeeComponent},
  {path: 'sup/edit/:id', component: EditSupervisorComponent},
  {path: 'emp/details/:id', component: DetailsEmployeeComponent},
  {path: 'sup/details/:id', component: DetailsSupervisorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
